**The paper *Why we should use balances and machine learning to diagnose ionomes* has only been published as preprint. For a more in-dept, peer-reviewed version, one should refer to Parent et al. (2020, https://doi.org/10.3390/plants9101401).**

# Why we should use balances and machine learning to diagnose ionomes

Serge-Étienne Parent

*Supplementary material*

## Abstract
The performance of a plant can be predicted from its ionome (concentration of elements in a living tissue) at a specific growth stage. Diagnoses have yet been based on simple statistical tools by relating a Boolean index to a vector of nutrient concentrations or to unstructured sets of nutrient ratios. We are now aware that compositional data such as nutrient concentrations should be carefully preprocessed before statistical modeling. Projecting concentrations to isometric log-ratios confer a Euclidean space to compositional data, similar to geographic coordinates. By comparing projected nutrient profiles to a geographical map, this perspective paper shows why univariate ranges and ellipsoids are less accurate to assess the nutrient     status of a plant from its ionome compared to machine learning models. I propose an imbalance index defined as the Aitchison distance between an imbalanced specimen to the closest balanced point or region in a reference data set. I also propose and raise some limitations of a recommendation system where the ionome of a specimen is translated to its closest point or region where high plant performance is reported. 

https://doi.org/10.22541/au.157954751.17355951
